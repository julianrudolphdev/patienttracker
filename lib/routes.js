Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function(){
    this.route('home', {
        path: '/',
        template: 'home',
         data: function(){
            templateData = {
                patients: Patients.find()
            }
            return templateData;
        }
    });
    
    //create new patient
    this.route('create_patient', {
        path: '/admin/create_patient',
        template: 'create_patient',
        onBeforeAction: function(){
      if(!Meteor.userId() || Meteor.userId() == null){
        Router.go('/')
      }
      this.next();
    }
    });
    
     this.route('patient_list', {
        path: '/admin/patient_list',
        template: 'patient_list',
        data: function(){
            templateData= {
                patients: Patients.find()
            }
            return templateData;
        },
        onBeforeAction: function(){
      if(!Meteor.userId() || Meteor.userId() == null){
        Router.go('/')
      }
      this.next();
    }
    });
    
    this.route('edit_patient', {
        path: 'admin/:_id/edit',
        template: 'edit_patient',
        data: function(){
            var currentPatient = this.params._id;
            return Patients.findOne({_id: currentPatient});
        },
        onBeforeAction: function(){
            if(!Meteor.userId() || Meteor.userId() == null){
            Router.go('/')
        }
      this.next();
    }
    });
    
});