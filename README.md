
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- I left it cause its cool!
    
    Julian Rudolph
    julianrudolph.com
    julian.rudolphdev@gmail.com


Hello, welcome to the readme of Patient Tracker.

This is meant to track patients through the OR and display their location to monitors in the waiting room.

But why?

I took a family member to surgery upon arival I was handed a piece of paper with a 6 digit number and a list of colors
and what they meant. The ID number showed up on the monitors, and the color is where they were in the process.  
I noticed that the system they were using was, for lack of a better term, pure crap. The numbers scrolled through but 
not at set intervals. Having to match a 6 digit number while it was scrolling 4 lines, then 2 lines, then 5 lines, made it difficult to
read. 

I changed the what and how, and made it easier.

1. Admin can CRUD patients
2. Admin sets the status, when admin edits the status...
3. the color of the patient changes on the screen.


