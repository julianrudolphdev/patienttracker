Meteor.publish('patients', function(){
    return Patients.find();
});

//methods on the server

Meteor.methods({
    addPatient : function(name, birthday, uid, status){
        //insert into db
        
        Patients.insert({
            name: name,
            birthday: birthday,
            uid: uid,
            status: status
        });
    },
    
    deletePatient: function(currentPatient){
        Patients.remove(currentPatient);
    },
    
    editPatient: function(currentPatient, name, birthday, uid, status){
        Patients.update({
            _id: currentPatient
        },{
            $set:{
                name: name,
                birthday: birthday,
                uid: uid,
                status: status
            }
        });
    }
});

