Template.create_patient.events({
    'submit .create_patient': function(event){
        event.preventDefault();
        //grab form input
        
        var name = event.target.name.value;
        var birthday = event.target.birthday.value;
        var uid = event.target.uid.value;
        var status = event.target.status.value;
        
       Meteor.call('addPatient', name, birthday, uid, status);
        
        //show message on success
        FlashMessages.sendSuccess("Patient added and being tracked!");
        Router.go('/admin/patient_list')
        
        //reset form
        event.target.name.value = '';
        event.target.birthday.value = '';
        event.target.uid.value = '';
        event.target.status.value = '';
    }
});

Template.edit_patient.events({
    'submit .edit_patient': function(event){
        event.preventDefault();
        //populate form with id info
        var currentPatient = this._id;
        
        //grab vars that may have changed
        var name = event.target.name.value;
        var birthday = event.target.birthday.value;
        var uid = event.target.uid.value;
        var status = event.target.status.value;
        
        Meteor.call('editPatient', currentPatient, name, birthday, uid, status);
        FlashMessages.sendSuccess("Patient Updated!");
        Router.go('/admin/patient_list');
    }
});


Template.patient_list.events({
    'click .delete_patient': function(event){
        event.preventDefault();
        var currentPatient = this._id;
        if(confirm("Delete Patient?")){
            Meteor.call('deletePatient', currentPatient);
            FlashMessages.sendSuccess("Patient Deleted");
        }
    }
});